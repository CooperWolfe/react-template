import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import NotFound from './notFound'

class Content extends Component {
  render() {
    return (
      <div className='container p-3'>
        <Switch>
          <Route path='/not-found' render={() => <NotFound className='c-accent' linkStyle={{ color: 'var(--light)' }}/>}/>
          <Redirect from='/' to='/not-found' />
        </Switch>
      </div>
    )
  }
}

export default Content
