import React, { Component } from 'react'
import { ToastContainer } from 'react-toastify'
import Content from './components/content'
import Footer from './components/footer'

class App extends Component {
  render() {
    return (
      <div className='app'>
        <ToastContainer position='top-center'/>
        <Content />
        <Footer />
      </div>
    )
  }
}

export default App
