import React from 'react'
import ReactDOM from 'react-dom'
import App from './app.jsx'
import { Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './stores/app.store'
import history from './services/history'
import * as serviceWorker from './serviceWorker'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/font-awesome/css/font-awesome.min.css'
import '../node_modules/react-toastify/dist/ReactToastify.min.css'
import './index.css'

ReactDOM.render(
  <Provider store={store}><Router history={history}><App /></Router></Provider>,
  document.getElementById('root')
)

// Change to register on final production
serviceWorker.unregister()
